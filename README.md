# Godot build image
Image to build a godot project for all configured platforms.

Latest tested Godot Version: **4.0.2**

### Supported platforms
* Linux
* Windows
* Android

### Android
#### Generate a release keystore
(replace `<yourpwd>`)
```
docker run --rm -v .:/var/key/ cyberrebell/godot-project-build keytool -v -genkey -keystore /var/key/release.keystore -alias release -keyalg RSA -validity 10000 -dname "CN=Android,O=Android,C=US" -storepass <yourpwd>
```
#### Pass your release key password
Put it into the ANDROID_RELEASE_KEY env variable

## Build for all platforms
Use the image from docker hub:
```
cyberrebell/godot-project-build
```

Run the command in your pipeline from your godot project directory
```godot_build_all_presets```

Output .zip files for all platform will be created into `.gdexport/`