FROM debian:stable-slim
ENV GODOT_VERSION=4.0.2
ENV GODOT_RELEASE_PATH=https://github.com/godotengine/godot/releases/download/${GODOT_VERSION}-stable/Godot_v${GODOT_VERSION}-stable_
ENV GODOT_EXPORT_TEMPLATE_DIR=/root/.local/share/godot/export_templates/${GODOT_VERSION}.stable/
ENV BLENDER_VERSION=3.5.0

RUN apt update && apt install -y wget unzip xz-utils zip && apt clean \
    && wget -O /tmp/godot.zip ${GODOT_RELEASE_PATH}linux.x86_64.zip \
    && unzip /tmp/godot.zip -d /tmp/ \
    && mv /tmp/Godot* /usr/bin/godot \
    && rm -R /tmp/*

RUN wget -O /tmp/export_templates.tpz ${GODOT_RELEASE_PATH}export_templates.tpz \
    && unzip /tmp/export_templates.tpz -d /tmp/ \
    && mkdir --parents ${GODOT_EXPORT_TEMPLATE_DIR} \
    && mv /tmp/templates/* ${GODOT_EXPORT_TEMPLATE_DIR} \
    && rm -R /tmp/*

RUN wget -O /tmp/blender.tar.xz https://ftp.halifax.rwth-aachen.de/blender/release/Blender3.5/blender-${BLENDER_VERSION}-linux-x64.tar.xz \
    && tar -xf /tmp/blender.tar.xz -C /tmp/ \
    && mv /tmp/blender-${BLENDER_VERSION}-linux-x64/ /opt/blender/ \
    && rm -R /tmp/*
RUN apt update && apt install -y xvfb libxrender1 libxi6 libxkbcommon-x11-0 && apt clean

COPY godot_build_all_presets /usr/bin/

RUN apt update && apt install -y openjdk-11-jdk android-sdk && apt clean

RUN wget -O /tmp/cmdline-tools.zip https://dl.google.com/android/repository/commandlinetools-linux-9477386_latest.zip \
    && unzip /tmp/cmdline-tools.zip -d /tmp/ \
    && yes | /tmp/cmdline-tools/bin/sdkmanager --licenses --sdk_root=/usr/lib/android-sdk/ \
    && /tmp/cmdline-tools/bin/sdkmanager --sdk_root=/usr/lib/android-sdk/ "platform-tools" "build-tools;33.0.2" "platforms;android-33" "cmdline-tools;latest" "cmake;3.10.2.4988404" "ndk;23.2.8568313" \
    && rm -R /tmp/* \
    && cd ~/.android/ && keytool -keyalg RSA -genkeypair -alias androiddebugkey -keypass android -keystore debug.keystore -storepass android -dname "CN=Android Debug,O=Android,C=US" -validity 9999 -deststoretype pkcs12

COPY editor_settings-4.tres /root/.config/godot/editor_settings-4.tres
